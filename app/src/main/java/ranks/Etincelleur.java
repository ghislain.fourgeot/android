package ranks;

import com.example.fourgeot.projet.Global;

public class Etincelleur extends ARanks {
    public Etincelleur(Global g) {
        super(10000, 0, g);
    }

    @Override
    public void refreshPriceNArgPSec()
    {
        argPSec += quantity == 1 ? 101 : 100;
        price = quantity * 10000;
    }
}
