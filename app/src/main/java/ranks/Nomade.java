package ranks;

import com.example.fourgeot.projet.Global;

public class Nomade extends ARanks
{

    public Nomade(Global g) {
        super(55, 0, g);
    }

    @Override
    public void refreshPriceNArgPSec()
    {
        argPSec += quantity == 1 ? 3.5 : 2.5;
        price = quantity * 55;

    }
}
