package ranks;

import com.example.fourgeot.projet.Global;

public class Sorcier extends ARanks {
    public Sorcier(Global g) {
        super(350, 0, g);
    }

    @Override
    public void refreshPriceNArgPSec()
    {
        argPSec += quantity == 1 ? 6 : 5;
        price = quantity * 350;
    }
}
