package ranks;

import com.example.fourgeot.projet.Global;

public class Conseille extends ARanks {
    public Conseille(Global g) {
        super(5000, 0, g);
    }

    @Override
    public void refreshPriceNArgPSec()
    {
        argPSec += quantity == 1 ? 45 : 44;
        price = quantity * 5000;
    }
}
