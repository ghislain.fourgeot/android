package ranks;

import com.example.fourgeot.projet.Global;

public class Soldat extends ARanks
{
    public Soldat(Global g) {
        super(10, 0, g);
    }

    public void refreshPriceNArgPSec()
    {
        argPSec += quantity == 1 ? 1.1 : 0.5;
        price = quantity * 10;;
    }
}
