package ranks;

import com.example.fourgeot.projet.Global;

public abstract class ARanks
{
    protected int quantity;
    protected double price;
    protected double argPSec;
    protected Global g;

    public void increaseQuantityBy(int incr)
    {
        quantity += incr;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public double getArgPSec() {
        return argPSec;
    }


    public ARanks(double price, double argPSec, Global g)
    {
        quantity = 0;
        this.price = price;
        this.argPSec = argPSec;
        this.g = g;
    }

    public abstract void refreshPriceNArgPSec();

    public void buyOne()
    {
        if(g.getArgentite() >= price)
        {
            g.rmArgentite(price);
            increaseQuantityBy(1);
            refreshPriceNArgPSec();
        }
    }
    public void setQuantity(int value){
        this.quantity=value;
    }


}
