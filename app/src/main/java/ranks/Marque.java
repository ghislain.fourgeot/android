package ranks;

import android.provider.Settings;

import com.example.fourgeot.projet.Global;

public class Marque extends ARanks {
    public Marque(Global g) {
        super(1500, 0, g);
    }

    @Override
    public void refreshPriceNArgPSec()
    {
        argPSec += quantity == 1 ? 20 : 19;
        price = quantity * 1500;
    }
}
