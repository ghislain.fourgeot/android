package com.example.fourgeot.projet;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import ranks.Conseille;
import ranks.Etincelleur;
import ranks.Marque;
import ranks.Nomade;
import ranks.Soldat;
import ranks.Sorcier;

public class MainActivity extends AppCompatActivity {

    private DBOpenHelper dbopenhelper;
    public final Global g = Global.getInstance();
    Handler h=new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        dbopenhelper = new DBOpenHelper(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.Main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                g.addArgentite(g.getArgPClick());
                refreshArg();
            }
        });

        h.post(new Runnable(){
            @Override
            public void run() {
                g.addArgentite(g.getArgPSec());
                refreshArg();
                h.postDelayed(this,500); // set time here to refresh textView
            }
        });


    }

    public void addSoldat(View v)
    {
        Soldat s = g.getSoldat();
        s.buyOne();
        refreshArg();
        g.refresh();
    }

    public void addNomade(View v)
    {
        Nomade n = g.getNomade();
        n.buyOne();
        refreshArg();
        g.refresh();
    }

    public void addSorcier(View v)
    {
        Sorcier s = g.getSorcier();
        s.buyOne();
        refreshArg();
        g.refresh();
    }

    public void addMarque(View v)
    {
        Marque m = g.getMarque();
        m.buyOne();
        refreshArg();
        g.refresh();
    }

    public void addConseille(View v)
    {
        Conseille c = g.getConseille();
        c.buyOne();
        refreshArg();
        g.refresh();
    }

    public void addEtincelleur(View v)
    {
        Etincelleur e = g.getEtincelleur();
        e.buyOne();
        refreshArg();
        g.refresh();
    }

    @SuppressLint("SetTextI18n")
    public void refreshArg()
    {

        TextView arg = (TextView)findViewById(R.id.Argentite);
        TextView pA = (TextView)findViewById(R.id.Art);

        TextView nbS = (TextView)findViewById(R.id.nbSldt);
        TextView pS = (TextView)findViewById(R.id.prixSldt);
        
        TextView nbN = (TextView)findViewById(R.id.nbNmds);
        TextView pN = (TextView)findViewById(R.id.prixNmds);
        
        TextView nbSo = (TextView)findViewById(R.id.nbSrcs);
        TextView pSo = (TextView)findViewById(R.id.prixSrcs);
        
        TextView nbM = (TextView)findViewById(R.id.nbMrqs);
        TextView pM = (TextView)findViewById(R.id.prixMrqs);
        
        TextView nbC = (TextView)findViewById(R.id.nbCns);
        TextView pC = (TextView)findViewById(R.id.prixCns);
        
        TextView nbE = (TextView)findViewById(R.id.nbEtls);
        TextView pE = (TextView)findViewById(R.id.prixEtls);


        arg.setText("Argentite: " + (int)g.getArgentite());
        Random r = new Random();
        g.addPA(r.nextInt(100) == 1 ? 1 * g.getArgPSec() / 10 : 0);
        pA.setText("PA: " + (int)g.getPA());

        nbS.setText(getString(R.string.soldier) +  (int)g.getSoldat().getQuantity());
        pS.setText(getString(R.string.price) + (int) g.getSoldat().getPrice());

        nbN.setText(getString(R.string.nomade) + g.getNomade().getQuantity());
        pN.setText(getString(R.string.price) + g.getNomade().getPrice());

        nbSo.setText(getString(R.string.sorcerer) + g.getSorcier().getQuantity());
        pSo.setText(getString(R.string.price) + g.getSorcier().getPrice());

        nbM.setText(getString(R.string.marque) + g.getMarque().getQuantity());
        pM.setText(getString(R.string.price) + g.getMarque().getPrice());

        nbC.setText(getString(R.string.conseille) + g.getConseille().getQuantity());
        pC.setText(getString(R.string.price) + g.getConseille().getPrice());

        nbE.setText(getString(R.string.etincelleur) + g.getEtincelleur().getQuantity());
        pE.setText(getString(R.string.price) + g.getEtincelleur().getPrice());
    }

    public void AccesUpgrade(View v){
        Log.d("UPGRADE","PONEY");
        setContentView(R.layout.upgrade);
    }

    public void AccesSettings(View v){
        setContentView(R.layout.setting);
    }

    public void AccesTrophies(View v){
        setContentView(R.layout.trophies);
    }

    public void Accescharacter(View v){
        setContentView(R.layout.character);
    }

    public void AccesMain(View v){
        setContentView(R.layout.activity_main);
        findViewById(R.id.Main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                g.addArgentite(g.getArgPClick());
                refreshArg();
            }
        });
    }

    public void AccesBob(View v){
        setContentView(R.layout.choosenone);
        ImageView Choosen = findViewById(R.id.choose);
        Choosen.setBackgroundResource(R.mipmap.bobf);
    }
    public void AccesAlberio(View v){
        setContentView(R.layout.choosenone);
        ImageView Choosen = findViewById(R.id.choose);
        Choosen.setBackgroundResource(R.mipmap.alberiof);
    }
    public void AccesTheo(View v){
        setContentView(R.layout.choosenone);
        ImageView Choosen = findViewById(R.id.choose);
        Choosen.setBackgroundResource(R.mipmap.theof);
    }
    public void AccesHarald(View v){
        setContentView(R.layout.choosenone);
        ImageView Choosen = findViewById(R.id.choose);
        Choosen.setBackgroundResource(R.mipmap.haraldf);
    }
    public void AccesMarian(View v){
        setContentView(R.layout.choosenone);
        ImageView Choosen = findViewById(R.id.choose);
        Choosen.setBackgroundResource(R.mipmap.marianf);
    }
    public void AccesDonnan(View v){
        setContentView(R.layout.choosenone);
        ImageView Choosen = findViewById(R.id.choose);
        Choosen.setBackgroundResource(R.mipmap.donnanf);
    }
    public void Save(View v){
        Log.d("SAVE","PONEY");
        EditText arg = (EditText) findViewById(R.id.Saveacces);
        dbopenhelper.addTable(String.valueOf(arg.getText()));
        dbopenhelper.insertValue( String.valueOf(g.getSoldat().getQuantity()),"nbsoldat",String.valueOf(arg.getText()));
        dbopenhelper.insertValue( String.valueOf(g.getNomade().getQuantity()),"nbnomade",String.valueOf(arg.getText()));
        dbopenhelper.insertValue( String.valueOf(g.getSorcier().getQuantity()),"nbsorcier",String.valueOf(arg.getText()));
        dbopenhelper.insertValue( String.valueOf(g.getMarque().getQuantity()),"nbmarque",String.valueOf(arg.getText()));
        dbopenhelper.insertValue( String.valueOf(g.getConseille().getQuantity()),"nbconseiller",String.valueOf(arg.getText()));
        dbopenhelper.insertValue( String.valueOf(g.getEtincelleur().getQuantity()),"nbetincelleur",String.valueOf(arg.getText()));

    }
    public void Load(View v){
        EditText arg = (EditText) findViewById(R.id.Saveacces);

        List<String> val=dbopenhelper.getValues("nbsoldat",String.valueOf(arg.getText()));
        String values = "";
        for(int i = 0 ; i < val.size() ;  ++i) {
            if (val.get(i)!=null){
                values=val.get(i);
            }
        }
        g.getSoldat().setQuantity((Integer.parseInt(values)));
        val=dbopenhelper.getValues("nbnomade",String.valueOf(arg.getText()));
        for(int i = 0 ; i < val.size() ;  ++i) {
            if (val.get(i)!=null){
                values=val.get(i);
            }
        }
        g.getNomade().setQuantity((Integer.parseInt(values)));

        val=dbopenhelper.getValues("nbsorcier",String.valueOf(arg.getText()));
        for(int i = 0 ; i < val.size() ;  ++i) {
            if (val.get(i)!=null){
                values=val.get(i);
            }
        }
        g.getSorcier().setQuantity((Integer.parseInt(values)));
        val=dbopenhelper.getValues("nbmarque",String.valueOf(arg.getText()));
        for(int i = 0 ; i < val.size() ;  ++i) {
            if (val.get(i)!=null){
                values=val.get(i);
            }
        }
        g.getMarque().setQuantity((Integer.parseInt(values)));
        val=dbopenhelper.getValues("nbconseiller",String.valueOf(arg.getText()));
        for(int i = 0 ; i < val.size() ;  ++i) {
            if (val.get(i)!=null){
                values=val.get(i);
            }
        }
        g.getConseille().setQuantity((Integer.parseInt(values)));
        val=dbopenhelper.getValues("nbetincelleur",String.valueOf(arg.getText()));
        for(int i = 0 ; i < val.size() ;  ++i) {
            if (val.get(i)!=null){
                values=val.get(i);
            }
        }
        g.getEtincelleur().setQuantity((Integer.parseInt(values)));
        refreshArg();
    }

}
