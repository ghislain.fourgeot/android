package com.example.fourgeot.projet;

import ranks.Conseille;
import ranks.Etincelleur;
import ranks.Marque;
import ranks.Nomade;
import ranks.Soldat;
import ranks.Sorcier;

/**
 * Created by fourgeot on 29/03/18.
 */

public class Global {
    private static Global instance;

    // Global variable
    private double argPClick;
    private double argPSec;
    private float prctAme;
    private float prctSpawnAeldrazi;
    private float prctGoodEvil;

    private double argentite;
    private double PA;
    private double Aeldrazi;
    private double Ame;
    private double good;
    private double evil;

    private Soldat soldat;
    private Nomade nomade;
    private Sorcier sorcier;
    private Marque marque;
    private Conseille conseille;
    private Etincelleur etincelleur;


    // Restrict the constructor from being instantiated
    private Global(){

        soldat = new Soldat(this);
        nomade = new Nomade(this);
        sorcier = new Sorcier(this);
        marque = new Marque(this);
        conseille = new Conseille(this);
        etincelleur = new Etincelleur(this);

        argPClick = 1;
        argPSec = 0;
        prctAme = 0.0f;
        prctSpawnAeldrazi = 0.01f;
        prctGoodEvil = 0.0f;

        argentite = 0;
        PA = 0;
        Aeldrazi = 0;
        Ame = 0;
        good = 0;
        evil = 0;
    }

    public Soldat getSoldat(){return this.soldat;}
    public Nomade getNomade(){return this.nomade;}
    public Sorcier getSorcier(){return this.sorcier;}
    public Marque getMarque(){return this.marque;}
    public Conseille getConseille(){return this.conseille;}
    public Etincelleur getEtincelleur(){return this.etincelleur;}


    public double getArgPSec(){
        return this.argPSec;
    }

    public void setArgPClick(double d){
        this.argPClick=d;
    }
    public double getArgPClick(){
        return this.argPClick;
    }
    public void addArgPClick(double value){
        this.argPClick+=value;
    }

    public void setPrctAme(float d){
        this.prctAme=d;
    }
    public double getPrctAme(){
        return this.prctAme;
    }
    public void addPrctAme(float value){
        this.prctAme+=value;
    }

    public void setPrctSpawnAeldrazi(float d){
        this.prctSpawnAeldrazi=d;
    }
    public double getPrctSpawnAeldrazi(){
        return this.prctSpawnAeldrazi;
    }
    public void addPrctSpawnAeldrazi(float value){
        this.prctSpawnAeldrazi+=value;
    }

    public void setPrctGoodEvil(float d){
        this.prctGoodEvil=d;
    }
    public double getPrctGoodEvil(){
        return this.prctGoodEvil;
    }
    public void addPrctGoodEvil(float value){
        this.prctGoodEvil+=value;
    }

    public void setArgentite(double d){
        this.argentite=d;
    }
    public double getArgentite(){
        return this.argentite;
    }
    public void addArgentite(double value){
        this.argentite+=value;
    }
    public void rmArgentite(double value){
        this.argentite-=value;
    }
    public void setPA(double d){
        this.PA=d;
    }
    public double getPA(){
        return this.PA;
    }
    public void addPA(double value){
        this.PA+=value;
    }
    public void setAeldrazi(double d){
        this.Aeldrazi=d;
    }
    public double getAeldrazi(){
        return this.Aeldrazi;
    }
    public void addAeldrazi(double value){
        this.Aeldrazi+=value;
    }
    public void setAme(double d){
        this.Ame=d;
    }
    public double getAme(){
        return this.Ame;
    }
    public void addAme(double value){
        this.Ame+=value;
    }
    public void setGood(double d){
        this.good=d;
    }
    public double getGood(){
        return this.good;
    }
    public void addGood(double value){
        this.good+=value;
    }
    public void setEvil(double d){
        this.evil=d;
    }
    public double getEvil(){
        return this.evil;
    }
    public void addEvil(double value){
        this.evil+=value;
    }

    public void refresh()
    {
        argPSec = soldat.getArgPSec()
                + nomade.getArgPSec()
                + sorcier.getArgPSec()
                + marque.getArgPSec()
                + conseille.getArgPSec()
                + etincelleur.getArgPSec();
    }


    public static synchronized Global getInstance(){
        if(instance==null){
            instance=new Global();
        }
        return instance;
    }
}
