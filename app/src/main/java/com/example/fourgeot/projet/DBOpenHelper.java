package com.example.fourgeot.projet;//package sjubertie.examples;


import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;

import android.content.ContentValues;
import android.util.Log;

import java.util.List;
import java.util.ArrayList;


public class DBOpenHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "DB";             // Nom de la base.
    private static final String DB_TABLE_NAME = "savefile";  // Nom de la table.

    private SQLiteDatabase db;                              // Base de données

    DBOpenHelper(Context context) {
	// Appel au constructeur qui s'occupe de créer ou ouvrir la base.
	super(context, DB_NAME, null, 2);
	// Récupération de la base de données.
	db = getWritableDatabase();
    }

    /**
     * Méthode appelée si la base n'existe pas.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
	db.execSQL("create table " + DB_TABLE_NAME+ " (_id integer primary key autoincrement, nbsoldat text,nbnomade text, nbsorcier text, nbmarque text, nbconseiller text, nbetincelleur text);");
    }

    /**
     * Méthode pour passer d'une version de SQLite à une nouvelle version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldversion, int newversion) {

    }
    public void addTable(String name){
        Log.d("TABLE","name");
        db.execSQL("create table " + name+ " (_id integer primary key autoincrement, nbsoldat text,nbnomade text, nbsorcier text, nbmarque text, nbconseiller text, nbetincelleur text);");
    }


    /**
     * Insertion d'une chaîne dans la table.
     */
    public void insertValue(String value,String column,String Table) {
        Log.d("INSERT","column");
	ContentValues content = new ContentValues();
	content.put(column, value);

	db.insert(Table, null, content);
    }

    /**
     * Récupération des chaînes de la table.
     */
    public List<String> getValues(String column,String Table) {
	List<String> list = new ArrayList<String>();
	String[] columns = {column};
	// Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
	Cursor cursor = db.query(Table, columns, null, null, null, null, null);
	// Curseur placé en début des chaînes récupérées.
	cursor.moveToFirst();
	while (!cursor.isAfterLast()) {
	    // Récupération d'une chaîne et insertion dans une liste.
	    list.add(cursor.getString(0));
	    // Passage à l'entrée suivante.
	    cursor.moveToNext();
	}
	// Fermeture du curseur.
	cursor.close();

	return list;
    }

}